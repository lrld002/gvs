//
//  ViewController.swift
//  GVS
//
//  Created by Gerardo García on 20/10/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    private let loginButton = FBSDKLoginButton()
    //@IBOutlet weak var loginButton = FBSDKLoginButton()
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Auth.auth().currentUser != nil {
            performSegue(withIdentifier: "continueToListSegue", sender: nil)
            return
        }
        
        setUpViews()
        
    }
    
    private func setUpViews() {
        loginButton.delegate = self
        let constraint1 = NSLayoutConstraint(item: loginButton, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0)
        let constraint2 = NSLayoutConstraint(item: loginButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loginButton)
        view.addConstraints([constraint1, constraint2])
    }    
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if error != nil {
                return
            }
            
            if let firebaseUser = Auth.auth().currentUser {
                AppDelegate.currentUser = User(id: firebaseUser.uid, payId: "", mail: firebaseUser.email ?? "", name: firebaseUser.displayName ?? "Anon", photoUrl: firebaseUser.photoURL?.absoluteString ?? "", gender: false, timestamp: Int64(NSDate().timeIntervalSince1970  * 1000))
                
                self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_USERS).document(AppDelegate.currentUser.id).setData(AppDelegate.currentUser.toData())
                
                let cart = ShoppingCart(id: AppDelegate.currentUser.id, user: AppDelegate.currentUser, items: [])
                self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_CARTS).document(cart.id).setData(cart.toData())
                print(AppDelegate.currentUser.toData())
                self.performSegue(withIdentifier: "continueToListSegue", sender: nil)
            }
        }
        if let error = error {
            print(error.localizedDescription)
            return
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        FBSDKLoginManager().logOut()
    }

}

