//
//  MedicineViewController.swift
//  GVS
//
//  Created by Gerardo García on 26/12/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import UIKit
import Nuke
import Firebase
import FirebaseFirestore

class MedicineViewController: UIViewController {

    @IBOutlet weak var profileMedicineImage: UIImageView!
    @IBOutlet weak var profileMedicineName: UILabel!
    @IBOutlet weak var profileMedicineDescription: UILabel!
    @IBOutlet weak var profileMedicineTag: UILabel!
    @IBOutlet weak var profileMedicineButton: UIButton!
    
    var currentMID: String!
    var currentMName: String!
    var currentMTag: String!
    var currentMImage: String!
    var currentMDescription: String!
    
    let db = Firestore.firestore()
    var currentItem: Item! {
        didSet {
            let url = URL.init(string: self.currentItem?.images?.first ?? AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)
            if url != nil && !url!.absoluteString.isEmpty {
                Nuke.loadImage(with: url!, into: self.profileMedicineImage)
            }
            self.profileMedicineName.text = self.currentItem?.name
            self.profileMedicineDescription.text = self.currentItem?.text
            self.profileMedicineTag.text = "$\(String(describing: self.currentItem?.price))"
            self.profileMedicineButton.isHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        medicineProfileConstraints()
        initViews()
    }
    
    private func initViews() {
        let query = db.collection(AppConstants.FIREBASE_DATABASE_TABLE_ITEMS).whereField("id", isEqualTo: currentMID)
        query.getDocuments { (snapshot, error) in
            if error != nil {
            } else {
                self.currentItem = Item(dictionary: snapshot?.documents.first?.data())
            }
        }
        
        //self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(back))
        
        /*profileMedicineName.text = currentMName
        
        profileMedicineDescription.text = currentMDescription
        profileMedicineDescription.numberOfLines = 10
        
        profileMedicineTag.text = "$" + currentMTag
        
        let url = URL.init(string: currentMImage)
        if (url != nil && !url!.absoluteString.isEmpty) {
            Nuke.loadImage(with: url!, into: profileMedicineImage)
        } else {
            Nuke.loadImage(with: URL(string: AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)!, into: profileMedicineImage)
        }*/
        
        profileMedicineButton.setTitle("Agregar al carrito", for: .normal)
    
    }
    
    @objc private func back() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    private func medicineProfileConstraints() {
        
        view.addSubview(profileMedicineImage)
        view.addConstraintWithVisualFormat(format: "H:|-120-[v0(160)]-120-|", views: profileMedicineImage)
        view.addConstraintWithVisualFormat(format: "V:|-100-[v0(160)]", views: profileMedicineImage)
        
        view.addSubview(profileMedicineName)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]", views: profileMedicineName)
        view.addConstraintWithVisualFormat(format: "V:|-320-[v0]", views: profileMedicineName)
        
        view.addSubview(profileMedicineDescription)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: profileMedicineDescription)
        view.addConstraintWithVisualFormat(format: "V:|-400-[v0]", views: profileMedicineDescription)
        
        view.addSubview(profileMedicineTag)
        view.addConstraintWithVisualFormat(format: "H:|-120-[v0(160)]-120-|", views: profileMedicineTag)
        view.addConstraintWithVisualFormat(format: "V:|-480-[v0(160)]", views: profileMedicineTag)
        
        view.addSubview(profileMedicineButton)
        view.addConstraintWithVisualFormat(format: "H:|-120-[v0(160)]-120-|", views: profileMedicineButton)
        view.addConstraintWithVisualFormat(format: "V:|-560-[v0(160)]", views: profileMedicineButton)
        
    }
    
    @IBAction func addedToCart(_ sender: UIButton) {
        AppDelegate.currentCart.items.append(currentItem)
        AppDelegate.publishCartChanges()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
