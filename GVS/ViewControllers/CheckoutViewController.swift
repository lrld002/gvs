//
//  CheckoutViewController.swift
//  GVS
//
//  Created by Gerardo García on 02/01/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import UIKit

class CheckoutViewController: UIViewController {
    
    @IBOutlet weak var cardNameEdit: UITextField!
    @IBOutlet weak var cardDigitsEdit: UITextField!
    @IBOutlet weak var cardExpirationEdit: UITextField!
    @IBOutlet weak var cardCodeEdit: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        
    }
    
    private func setUpViews() {
        cardNameEdit.placeholder = "Nombre de la tarjeta"
        cardDigitsEdit.placeholder = "Números de la tarjeta"
        cardExpirationEdit.placeholder = "Fecha de expiracion"
        cardCodeEdit.placeholder = "CVV"
    }

}
