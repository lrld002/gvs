//
//  CartCollectionViewController.swift
//  GVS
//
//  Created by Gerardo García on 26/12/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class CartCollectionViewController: UICollectionViewController {

    @IBOutlet var cartCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        cartCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        // Do any additional setup after loading the view.
    }
    
    private func setUpViews() {
        
        let nib = UINib.init(nibName: "collectionMedicineCell", bundle: nil)
        cartCollectionView.register(nib, forCellWithReuseIdentifier: "collection_medicine_cell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: view.frame.width, height: 150)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        cartCollectionView!.collectionViewLayout = layout
        
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_medicine_cell", for: indexPath) as! CollectionMedicineCell
        
        cell.medicineView.backgroundColor = .green
        cell.medicineImage.backgroundColor = .red
        
        cell.addSubview(cell.medicineView)
        cell.addConstraintWithVisualFormat(format: "H:|[v0]|", views: cell.medicineView)
        cell.addConstraintWithVisualFormat(format: "V:|-2-[v0(600)]-2-|", views: cell.medicineView)
        
        cell.addSubview(cell.medicineImage)
        cell.addConstraintWithVisualFormat(format: "H:|-8-[v0(70)]", views: cell.medicineImage)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0(70)]-8-|", views: cell.medicineImage)
        
        cell.addSubview(cell.medicineName)
        cell.addConstraintWithVisualFormat(format: "H:|-88-[v0]", views: cell.medicineName)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0]", views: cell.medicineName)
        
        cell.addSubview(cell.medicineDescription)
        cell.addConstraintWithVisualFormat(format: "H:|-88-[v0]", views: cell.medicineDescription)
        cell.addConstraintWithVisualFormat(format: "V:|-56-[v0]", views: cell.medicineDescription)
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "show_medicine_main", sender: self)
        /*if let pair = dataSource[indexPath.row] as? (user: User, post: Post) {
         selectedPID = pair.1.pid
         self.performSegue(withIdentifier: "show_post_main", sender: self)
         print("selectedPID")
         }*/
    }

}
