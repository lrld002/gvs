//
//  AccountViewController.swift
//  GVS
//
//  Created by Gerardo García on 26/12/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import UIKit

import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import Openpay

class AccountViewController: UIViewController {
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbMail: UILabel!
    
    var openpay: Openpay!
    var sessionId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        openpay = Openpay(withMerchantId: AppConstants.OPENPAY_MERCHANT_ID, andApiKey: AppConstants.OPENPAY_PUBLIC_KEY, isProductionMode: false, isDebug: true)
        
        
        lbName.text = AppDelegate.currentUser.name
        lbMail.text = AppDelegate.currentUser.mail
        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func addCardClicked(_ sender: UIButton) {
        openpay.createDeviceSessionId(successFunction: doJob, failureFunction: failure)
    }
    

    func doJob(sessionID: String){
        openpay.loadCardForm(in: self, successFunction: successCard, failureFunction: failCard, formTitle: "Add card")
    }
    
    func failure(error: NSError){
        
    }
    
    func successCard() {
    }
    
    func failCard(error: NSError) {
        print("Fail Card Capture...")
        print("\(error.code) - \(error.localizedDescription)")
    }
}
