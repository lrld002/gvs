//
//  MainTabBarController.swift
//  GVS
//
//  Created by Gerardo García on 26/12/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cart"), style: .done, target: self, action: #selector(cart))

    }
    
    private func setUpViews() {
        self.tabBar.isTranslucent = false
        self.tabBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    @objc func cart() {
        self.performSegue(withIdentifier: "show_cart_main", sender: self)
    }

}
