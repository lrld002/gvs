//
//  HomeCollectionViewController.swift
//  GVS
//
//  Created by Gerardo García on 26/12/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseUI
import Nuke

private let reuseIdentifier = "Cell"

class HomeCollectionViewController: UICollectionViewController {
    
    @IBOutlet var medicineCollectionView: UICollectionView!
    var collectionViewDataSource: FUIFirestoreCollectionViewDataSource!
    
    var selectedMID: String?
    var selectedMName: String?
    var selectedMTag: String?
    var selectedMImage: String?
    var selectedMDescription: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        setUpViews()
        // Do any additional setup after loading the view.
    }
    
    private func setUpViews() {
        let nib = UINib.init(nibName: "collectionMedicineCell", bundle: nil)
        medicineCollectionView.register(nib, forCellWithReuseIdentifier: "collection_medicine_cell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: view.frame.width, height: 150)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        medicineCollectionView!.collectionViewLayout = layout
        

        collectionViewDataSource = medicineCollectionView.bind(toFirestoreQuery: AppDelegate.itemsQuery()) { (collectionView, indexPath, snapshot) -> UICollectionViewCell in
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_medicine_cell", for: indexPath) as? CollectionMedicineCell {
                cell.addSubview(cell.medicineView)
                cell.addConstraintWithVisualFormat(format: "H:|[v0]|", views: cell.medicineView)
                cell.addConstraintWithVisualFormat(format: "V:|-2-[v0(600)]-2-|", views: cell.medicineView)
            
                cell.addSubview(cell.medicineImage)
                cell.addConstraintWithVisualFormat(format: "H:|-8-[v0(70)]", views: cell.medicineImage)
                cell.addConstraintWithVisualFormat(format: "V:|-8-[v0(70)]-8-|", views: cell.medicineImage)
            
                cell.addSubview(cell.medicineName)
                cell.addConstraintWithVisualFormat(format: "H:|-88-[v0]", views: cell.medicineName)
                cell.addConstraintWithVisualFormat(format: "V:|-8-[v0]", views: cell.medicineName)
            
                cell.addSubview(cell.medicineDescription)
                cell.addConstraintWithVisualFormat(format: "H:|-88-[v0]", views: cell.medicineDescription)
                cell.addConstraintWithVisualFormat(format: "V:|-56-[v0]", views: cell.medicineDescription)
            
                let item = Item(dictionary: self.collectionViewDataSource.items[indexPath.row].data())
                
                //let item = Item(dictionary: snapshot.data() )
                print(item)
                
                let url = URL.init(string: item.images?.first ?? AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)
                if (url != nil && !url!.absoluteString.isEmpty) {
                    Nuke.loadImage(with: url!, into: cell.medicineImage)
                } else {
                    Nuke.loadImage(with: URL(string: AppConstants.GVS_DEFAULT_PROFILE_IMAGE_URL)!, into: cell.medicineImage)
                }
                cell.medicineName.text = item.name
                cell.medicineDescription.text = "$" + String(item.price)
            
                return cell
            } else {
                return UICollectionViewCell()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "show_medicine_main") {
            let goMedicine = segue.destination as! MedicineViewController
            goMedicine.currentMID = self.selectedMID
            goMedicine.currentMName = self.selectedMName
            goMedicine.currentMTag = self.selectedMTag
            goMedicine.currentMImage = self.selectedMImage
            goMedicine.currentMDescription = self.selectedMDescription
        }
    }
    
    /*override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        /*self.dataSource = collectionView.bind(toFirestoreQuery: baseQuery()) {
            collectionView, indexPath, snap in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_medicine_cell", for: indexPath) as! CollectionMedicineCell
            return cell
        }*/
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_medicine_cell", for: indexPath) as! CollectionMedicineCell
    
        cell.medicineView.backgroundColor = .green
        cell.medicineImage.backgroundColor = .red
        
        cell.addSubview(cell.medicineView)
        cell.addConstraintWithVisualFormat(format: "H:|[v0]|", views: cell.medicineView)
        cell.addConstraintWithVisualFormat(format: "V:|-2-[v0(600)]-2-|", views: cell.medicineView)
        
        cell.addSubview(cell.medicineImage)
        cell.addConstraintWithVisualFormat(format: "H:|-8-[v0(70)]", views: cell.medicineImage)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0(70)]-8-|", views: cell.medicineImage)
        
        cell.addSubview(cell.medicineName)
        cell.addConstraintWithVisualFormat(format: "H:|-88-[v0]", views: cell.medicineName)
        cell.addConstraintWithVisualFormat(format: "V:|-8-[v0]", views: cell.medicineName)
        
        cell.addSubview(cell.medicineDescription)
        cell.addConstraintWithVisualFormat(format: "H:|-88-[v0]", views: cell.medicineDescription)
        cell.addConstraintWithVisualFormat(format: "V:|-56-[v0]", views: cell.medicineDescription)
    
        return cell
    }*/

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = Item(dictionary: collectionViewDataSource.items[indexPath.row].data())
        selectedMID = item.id
        selectedMName = item.name
        selectedMTag = String(item.price)
        selectedMImage = item.images?.first
        selectedMDescription = item.text
        performSegue(withIdentifier: "show_medicine_main", sender: self)
    }
    
}
