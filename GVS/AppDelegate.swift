//
//  AppDelegate.swift
//  GVS
//
//  Created by Gerardo García on 20/10/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    public static var currentCart: ShoppingCart = ShoppingCart(id: "", user: User(), items: [])
    public static var currentUser: User = User()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if Auth.auth().currentUser == nil {
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC")
            self.window?.rootViewController = initialViewController
        } else {
            db.collection(AppConstants.FIREBASE_DATABASE_TABLE_USERS).document(Auth.auth().currentUser?.uid ?? "").addSnapshotListener { (snapshot, error) in
                if error != nil {
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC")
                    self.window?.rootViewController = initialViewController
                }else {
                    AppDelegate.currentUser = User(dictionary: snapshot?.data())
                    db.collection(AppConstants.FIREBASE_DATABASE_TABLE_CARTS).document(AppDelegate.currentUser.id).addSnapshotListener { (snp, err) in
                        AppDelegate.currentCart = ShoppingCart(dictionary: snp?.data())
                        AppDelegate.currentCart.user = AppDelegate.currentUser
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainNC")
                        self.window?.rootViewController = initialViewController
                    }
                }
            }
        }
        let emptyViewController = storyboard.instantiateViewController(withIdentifier: "EmptyVC")
        self.window?.rootViewController = emptyViewController
        self.window?.makeKeyAndVisible()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        // Add any custom logic here.
        return handled
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    public static func publishCartChanges(){
        let db = Firestore.firestore()
        db.collection(AppConstants.FIREBASE_DATABASE_TABLE_CARTS).document(AppDelegate.currentCart.id).setData(AppDelegate.currentCart.toData())
    }

    public static func itemsQuery() -> Query {
        return Firestore.firestore().collection(AppConstants.FIREBASE_DATABASE_TABLE_ITEMS)
    }
    
    public static func cartQuery() -> Query {
        return Firestore.firestore().collection(AppConstants.FIREBASE_DATABASE_TABLE_CARTS).whereField("id", isEqualTo: AppDelegate.currentUser.id).limit(to: 1)
    }
}

