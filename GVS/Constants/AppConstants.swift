//
//  FirebaseDatabaseConstants.swift
//  GVS
//
//  Created by Gerardo García on 26/12/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import Foundation
import FirebaseDatabase

class AppConstants: NSObject {
    
    public static let OPENPAY_MERCHANT_ID = "mf8qp3qjsqvaepsemsll"
    public static let OPENPAY_PUBLIC_KEY = "pk_a57ea33e5bec423eb72996cd04fdea73"
    public static let OPENPAY_PRIVATE_KEY = "sk_4b57b15bc071435e87f878325f8a0d92"
    
    #if DEBUG
        public static let OPENPAY_IS_PRODUCTION = false
    #else
        public static let OPENPAY_IS_PRODUCTION = true
    #endif
    
    public static let GVS_DEFAULT_PROFILE_IMAGE_URL = "https://firebasestorage.googleapis.com/v0/b/farmacia-gvs.appspot.com/o/static%2Fproximamente.jpeg?alt=media&token=c5f06878-0809-4453-9230-e3360dfa1b4c"
    
    public static let FIREBASE_DATABASE_TABLE_ITEMS = "items"
    public static let FIREBASE_DATABASE_TABLE_USERS = "users"
    public static let FIREBASE_DATABASE_TABLE_CARTS = "carts"
    
}
