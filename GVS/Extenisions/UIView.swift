//
//  UIView.swift
//  GVS
//
//  Created by Gerardo García on 21/10/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func addConstraintWithVisualFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
