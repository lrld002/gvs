//
//  CollectionMedicineCell.swift
//  GVS
//
//  Created by Gerardo García on 26/12/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import UIKit

class CollectionMedicineCell: UICollectionViewCell {
    
    @IBOutlet weak var medicineView: UIView!
    @IBOutlet weak var medicineImage: UIImageView!
    @IBOutlet weak var medicineName: UILabel!
    @IBOutlet weak var medicineDescription: UILabel!
}
