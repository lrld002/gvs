//
//  FirebaseObject.swift
//  GVS
//
//  Created by Alain Peralta on 1/13/19.
//  Copyright © 2019 Sgifer. All rights reserved.
//

import Foundation

protocol FirebaseObject {
    func toData() -> [String: Any];
}
