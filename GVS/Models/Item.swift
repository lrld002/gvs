//
//  Item.swift
//  GVS
//
//  Created by Alain Peralta on 12/26/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import Foundation

class Item: FirebaseObject {
    var id: String!
    var name: String!
    var service: Bool!
    var price = 0.0
    var text: String!
    var department: String!
    var category: [String]!
    var prescription = false
    var tax = false
    var images: [String]?
    
    init(id: String, name: String, service: Bool, price: Double, text: String, department: String, category: [String], prescription: Bool, tax: Bool, images: [String]) {
        self.id = id
        self.name = name
        self.service = service
        self.price = price
        self.text = text
        self.department = department
        self.category = category
        self.prescription = prescription
        self.tax = tax
        self.images = images
    }
    
    init(dictionary: [String: Any]?) {
        self.id = dictionary?["id"] as? String
        self.name = dictionary?["name"] as? String
        self.service = dictionary?["service"] as? Bool
        self.price = dictionary?["price"] as? Double ?? 0.0
        self.text = dictionary?["text"] as? String
        self.department = dictionary?["department"] as? String
        self.category = dictionary?["category"] as? [String]
        self.prescription = dictionary?["prescription"] as? Bool ?? false
        self.tax = dictionary?["tax"] as? Bool ?? true
        self.images = dictionary?["images"] as? [String]
    }
    
    func toData() -> [String : Any] {
        let data = [
            "id": id,
            "name": name,
            "service": service,
            "price": price,
            "text": text,
            "department": department,
            "category": category,
            "prescription": prescription,
            "tax": tax,
            "images": images ?? []
            ] as [String: Any]
        
        return data
    }
}
