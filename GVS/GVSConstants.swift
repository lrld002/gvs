//
//  GVSConstants.swift
//  GVS
//
//  Created by Alain Peralta on 12/26/18.
//  Copyright © 2018 Sgifer. All rights reserved.
//

import Foundation

class GVSConstants {
    static let GVS_DEFAULT_MEDICINE_IMAGE_URL = "https://firebasestorage.googleapis.com/v0/b/farmacia-gvs.appspot.com/o/static%2Fproximamente.jpeg?alt=media&token=c5f06878-0809-4453-9230-e3360dfa1b4c"
    
    static let FIRESTORE_COLLECTION_ITEMS = "items"
    static let FIRESTORE_COLLECTION_USERS = "users"
    static let FIRESTORE_COLLECTION_SHOPPING = "shoppingCarts"
}
